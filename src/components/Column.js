import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { DropTarget } from 'react-dnd';
import { v4 as uuid } from 'uuid';
import itemTypes from '../constants/itemTypes';
import Element from './Element';

class Column extends Component {
  static propTypes = {
    elements: PropTypes.array,
    canDrop: PropTypes.bool.isRequired,
    isOver: PropTypes.bool.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    actions: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    rowId: PropTypes.string.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = { elements: props.elements };
  }

  pushElement = (elem) => {
    const { id, rowId } = this.props;
    elem.uuid = uuid();
    this.props.actions.addElement(elem, id, rowId);
  }

  removeElement = (element, sourceContainerId, targetContainerId) => {
    if (sourceContainerId !== targetContainerId) {
      this.props.actions.removeElement(element, sourceContainerId, this.props.rowId);
    }
  }

  moveElement = (sourceElement, targetElement, targetContainer) => {
    this.props.actions.switchElements(sourceElement, targetElement, targetContainer, this.props.rowId);
  }

  alignmentState = (rowId, containerId, elementUUID, alignment) => {
    this.props.actions.updateElementAlignment(rowId, containerId, elementUUID, alignment);
  }

  deleteColumn(columnId, rowId) {
    this.props.actions.removeColumn(columnId, rowId);
  }

  render() {
    const { elements, id, rowId } = this.props;
    const { canDrop, isOver, connectDropTarget } = this.props;
    const isActive = canDrop && isOver;

    return connectDropTarget(
      <div
        className={classNames('box', 'box__dashed', 'box-column', {
          box__active: isActive,
          box__inactive: !isActive,
        })}>
        <i onClick={() => this.deleteColumn(id, rowId)} className="pull-right glyphicon glyphicon-remove-circle button-close" />
        {elements &&
          elements.map((elem, index) => {
            return (
              <Element
                key={elem.uuid}
                index={index}
                containerId={id}
                elem={elem}
                rowId={rowId}
                removeElement={this.removeElement}
                moveElement={this.moveElement}
                alignmentState={this.alignmentState}
              />
            );
          })}
      </div>
    );
  }
}

const elemTarget = {
  drop(props, monitor, component) {
    const { id, elements, rowId } = props;
    const sourceObj = monitor.getItem();

    const elementUUIDS = elements.map(element => element.uuid);

    if (!elementUUIDS.includes(sourceObj.elem.uuid)) {
      component.pushElement(sourceObj.elem);
    }

    return {
      id,
      containerId: id,
      rowId,
    };
  },
};

export default DropTarget(itemTypes.ELEMENT, elemTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop(),
}))(Column);
