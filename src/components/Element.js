import React, { Component, PropTypes } from 'react';
import { DragSource, DropTarget } from 'react-dnd';
import { ButtonGroup, Button } from 'react-bootstrap';
import { compose } from 'redux';
import itemTypes from '../constants/itemTypes';

class Element extends Component {
  static propTypes = {
    elem: PropTypes.object.isRequired,
    isDragging: PropTypes.bool.isRequired,
    connectDragSource: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    removeElement: PropTypes.func.isRequired,
    alignmentState: PropTypes.func.isRequired,
    containerId: PropTypes.oneOfType([
      PropTypes.string.isRequired,
      PropTypes.number.isRequired]),
    rowId: PropTypes.oneOfType([
      PropTypes.string.isRequired,
      PropTypes.number.isRequired]),
  }

  constructor() {
    super();
    this.state = {
      alignment: 'left'
    };
  }

  setElementWidth = () => {
    const { containerId } = this.props;
    if (containerId === 10) {
      return '33%';
    }
    return '';
  }

  updateAlignment = (alignment) => {
    const { rowId, containerId, elem } = this.props;
    this.props.alignmentState(rowId, containerId, elem.uuid, alignment);
  }

  deleteElement = (element, sourceContainerId) => {
    this.props.removeElement(element, sourceContainerId);
  }

  render() {
    const { elem, isDragging, connectDragSource, connectDropTarget, containerId } = this.props;
    const opacity = isDragging ? 0 : 1;
    return compose(connectDragSource, connectDropTarget)(
      <div className="drag-element" style={{ opacity, width: this.setElementWidth() }}>
        <span className="drag-element__title">{elem.text}</span>
        <i
          onClick={() => this.deleteElement(elem, containerId)}
          className="pull-right glyphicon glyphicon-remove-circle button-close" aria-hidden="true" />
        <div>
          {elem.alignment &&
            <ButtonGroup className="drag-element__buttons">
              <Button bsSize="xsmall" onClick={() => this.updateAlignment('left')} active={elem.alignment === 'left'}>Vänster</Button>
              <Button bsSize="xsmall" onClick={() => this.updateAlignment('center')} active={elem.alignment === 'center'}>Centrera</Button>
              <Button bsSize="xsmall" onClick={() => this.updateAlignment('right')} active={elem.alignment === 'right'}>Höger</Button>
            </ButtonGroup>
          }
        </div>
      </div>
    );
  }
}

const elemSource = {
  // eslint-disable-next-line
  beginDrag(props, source) {
    return {
      rowId: props.rowId,
      containerId: props.containerId,
      id: props.id,
      elem: props.elem
    };
  },

  endDrag(props, monitor) {
    const item = monitor.getItem();
    const dropResult = monitor.getDropResult();
    const sourceContainerId = props.containerId;
    const draggedItemId = props.id;

    let isInContainer = false;
    if (dropResult && dropResult.elements) {
      isInContainer = dropResult.elements.includes(item.elem.uuid);
    }

    if ((dropResult && draggedItemId === item.id && !isInContainer) || (dropResult && dropResult.id === 10)) {
      const targetContainerId = dropResult.containerId;
      props.removeElement(item.elem, sourceContainerId, targetContainerId);
    }
  }
};


const elemTarget = {
  hover(targetProps, monitor) {
    const sourceProps = monitor.getItem();
    const sourceId = sourceProps.elem.uuid;
    const targetId = targetProps.elem.uuid;
    const targetContainer = targetProps.containerId;
    const sourceContainer = sourceProps.containerId;
    if (sourceId !== targetId && targetContainer === sourceContainer) {
      targetProps.moveElement(sourceProps.elem, targetProps.elem, targetContainer);
    }
  }
};

export default compose(
  DragSource(itemTypes.ELEMENT, elemSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
  })),
  DropTarget(itemTypes.ELEMENT, elemTarget, connect => ({
    connectDropTarget: connect.dropTarget()
  }))
)(Element);
