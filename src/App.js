import React, { Component, PropTypes } from 'react';
import { DragDropContext } from 'react-dnd';
import MultiBackend from 'react-dnd-multi-backend';
import HTML5toTouch from 'react-dnd-multi-backend/lib/HTML5toTouch';
import { Row, Col, Button, ButtonToolbar, ButtonGroup } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { v4 as uuid } from 'uuid';
import { CompactPicker } from 'react-color';
import 'bootstrap/dist/css/bootstrap.css';
import * as dndActions from './actions/dndActions';
import Column from './components/Column';
import Elements from './components/Elements';
import './App.css';

class App extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    dnd: PropTypes.shape({
      elements: PropTypes.array,
      columns: PropTypes.array,
      rows: PropTypes.array,
      backgroundColor: PropTypes.string.isRequired,
    }),
  };

  newRow = () => {
    this.props.actions.addRow({ id: uuid(), columns: [] });
  }

  deleteRow = (rowId) => {
    this.props.actions.removeRow(rowId);
  }

  newColumn = (size, rowId) => {
    const colToAdd = {
      id: uuid(),
      elements: [],
      size: size,
    };

    this.props.actions.addColumn(colToAdd, rowId);
  }

  generateHTML = () => {
    this.props.actions.generateHTML();
  }

  clearLayout = () => {
    this.props.actions.clearLayout();
  }

  handleColorChange = (color) => {
    this.props.actions.setBackgroundColor(color);
  }

  render() {
    const placeholders = [
      { id: '1', uuid: '', text: 'Text', alignment: 'left' },
      { id: '2', uuid: '', text: 'Image', alignment: 'left' },
      { id: '3', uuid: '', text: 'Video', alignment: 'left' },
      { id: '4', uuid: '', text: 'Header', alignment: 'left' },
      { id: '5', uuid: '', text: 'Content', alignment: false },
      { id: '6', uuid: '', text: 'RSS', alignment: false },
      { id: '7', uuid: '', text: 'Iframe', alignment: false },
    ];

    const { actions, dnd } = this.props;
    return (
      <div className="App">
        <Row>
          <Col xs={5}>
            <div className="flexbox-column">
              <h4 className="colors-title">Bakgrunds färg</h4>
              <CompactPicker width={200} color={dnd.backgroundColor} onChangeComplete={this.handleColorChange} />
            </div>
            <ButtonToolbar>
              <Button bsStyle="primary" onClick={this.newRow}>Ny Rad</Button>
              <Button bsStyle="success" id="genHTML" onClick={this.generateHTML}>Generera HTML/ASP.NET</Button>
              <Button bsStyle="danger" id="genHTML" onClick={this.clearLayout}>Nollställ</Button>
            </ButtonToolbar>
          </Col>
          <Col xs={7}>
            <Elements id={10} rowId={100} elements={placeholders} actions={actions} />
          </Col>
        </Row>

        {dnd.rows &&
          dnd.rows.map(row => {
            return (
              <div key={uuid()} className="box-container box__dashed" style={{ backgroundColor: dnd.backgroundColor }}>
                <Row>
                  <i onClick={() => this.deleteRow(row.id)} className="glyphicon glyphicon-remove-circle row-delete" />
                </Row>
                <Row key={row.id}>

                  {row.columns &&
                    row.columns.map(column => {
                      return (
                        <Col key={column.id} xs={column.size}>
                          <span>Storlek: {column.size / 2} / 6</span>
                          <Column
                            backgroundColor={dnd.backgroundColor}
                            id={column.id}
                            rowId={row.id}
                            elements={column.elements}
                            actions={actions}
                          />
                        </Col>
                      );
                    })}
                </Row>
                <ButtonGroup>
                  <h5 className="addcolumn-title">Lägg till kolumn</h5>
                  <Button onClick={() => this.newColumn(2, row.id)}>1/6</Button>
                  <Button onClick={() => this.newColumn(4, row.id)}>2/6</Button>
                  <Button onClick={() => this.newColumn(6, row.id)}>3/6</Button>
                  <Button onClick={() => this.newColumn(8, row.id)}>4/6</Button>
                  <Button onClick={() => this.newColumn(10, row.id)}>5/6</Button>
                  <Button onClick={() => this.newColumn(12, row.id)}>6/6</Button>
                </ButtonGroup>
              </div>
            );
          })
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    dnd: state.dnd,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(dndActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DragDropContext(MultiBackend(HTML5toTouch))(App));
